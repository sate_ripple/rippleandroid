package mil.afrl.discoverylab.sate13.rippleandroid.data.provider.util;

public interface ColumnMetadata {

    public int getIndex();

    public String getName();

    public String getType();
}
