package mil.afrl.discoverylab.sate13.rippleandroid.data.exception;

import com.foxykeep.datadroid.exception.CustomRequestException;

/**
 * Created by burt on 7/3/13.
 */
public class MyCustomRequestException extends CustomRequestException {

    private static final long serialVersionUID = -8215064867471739222L;

}
